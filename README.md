## Redfin Mobile Project

**Build Instructions**
* Clone Project and Use Android Studio 4.0
* Build in Android Studio. Should require nothing more out of the box.

**Features**
* The Action Bar has a maps button. Uses Google Maps SDK to display a list of food trucks and their locations.

**Your Focus Areas.**

Software Architecture, Code Style, Readability, Robustness and using standard Android development practices.
* Modularization
* Kotlin gradle buildscripts
* Rotation works
* Coroutines
* Navigation Component

**Copied-in code or copied-in dependencies.**

I took code for the DI setup, the error-handling stack and dependencies from past projects on a private repo. The idea for the domain mapping came from [this project](https://github.com/cobeisfresh/CleanArchitecture-Android-Showcase).

**What did I not focus on**
* Would create more views for empty, different error states, and no internet states. I opted to use Toasts for the sake of time


### Screenshots
<table>
    <tr>
        <td><img style="width: 350px: height:auto; margin: 0 50px" src="screenshots/horizontal.png"></img></td>
        <td><img src="screenshots/vertical.png"></img></td>
    </tr>
    <tr>
        <td align="center"><b>Horizontal</b></img></td>
        <td align="center"><b>Vertical</b></img></td>
    </tr>
</table>