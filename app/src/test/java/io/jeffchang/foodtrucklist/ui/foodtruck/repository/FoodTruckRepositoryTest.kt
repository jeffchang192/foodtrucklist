package io.jeffchang.foodtrucklist.ui.foodtruck.repository

import com.nhaarman.mockitokotlin2.*
import io.jeffchang.foodtrucklist.data.model.FoodTruck.FoodTruck
import io.jeffchang.foodtrucklist.data.model.FoodTruck.Region
import io.jeffchang.foodtrucklist.data.model.FoodTruck.Response
import io.jeffchang.foodtrucklist.data.service.FoodTruckService
import io.jeffchang.core.Failure
import io.jeffchang.core.Success
import io.jeffchang.core.TestContextProvider
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Test
import retrofit2.HttpException

class FoodTruckRepositoryTest {

    private val foodTruckService: FoodTruckService = mock()

    private val FoodTruckRepository = DefaultFoodTruckRepository(
        TestContextProvider(),
        foodTruckService
    )

    @Test
    fun `getFoodTruckes returns list of FoodTruckes upon success`() {
        runBlocking {
            // Given

            // When
            whenever(foodTruckService.getFoodTrucks(any(), any())).doReturn(
                Response(
                    total = 1,
                    region = Region(),
                    FoodTruckes = listOf(
                        FoodTruck()
                    )
                )
            )
            val result = FoodTruckRepository.getFoodTruckes("Los Angeles")

            // Then
            result shouldBeInstanceOf Success::class
            verify(foodTruckService, times(1)).getFoodTrucks(any(), any())
        }
    }

    @Test
    fun `getFoodTruckes on http exception returns failure`() {
        runBlocking {
            // Given

            // When
            whenever(foodTruckService.getFoodTrucks(any(), any())).thenThrow(
                HttpException(
                    retrofit2.Response.error<Response>(
                        404, ResponseBody.create(null, "")
                    )
                )
            )
            val result = FoodTruckRepository.getFoodTruckes("Los Angeles")

            // Then
            result shouldBeInstanceOf Failure::class
            verify(foodTruckService, times(1)).getFoodTrucks(any(), any())
        }
    }

}