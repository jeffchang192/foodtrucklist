package io.jeffchang.foodtrucklist.ui.foodtruck.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.jeffchang.foodtrucklist.data.model.FoodTruck.FoodTruck
import io.jeffchang.core.*
import io.jeffchang.core.data.ViewState
import io.jeffchang.foodtrucklist.ui.foodtruck.repository.FoodTruckRepository
import io.jeffchang.foodtrucklist.ui.foodtruck.usecase.DefaultGetFoodTruckUseCase
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class FoodTruckViewModelTest {

    private val coroutineContext = TestContextProvider()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Test
    fun `non-empty FoodTruck list is success viewstate`() {
        runBlocking {
            val FoodTruckRepository: FoodTruckRepository = mock()
            whenever(FoodTruckRepository.getFoodTruckes(any())).doReturn(Success(listOf(FoodTruck())))

            val useCase = DefaultGetFoodTruckUseCase(FoodTruckRepository)

            val FoodTruckViewModel = FoodTruckViewModel(
                this@FoodTruckViewModelTest.coroutineContext, useCase
            )

            FoodTruckViewModel.viewState().value shouldBeInstanceOf ViewState.Success::class
        }
    }

    @Test
    fun `empty FoodTruck list is empty viewstate`() {
        runBlocking {
            val FoodTruckRepository: FoodTruckRepository = mock()
            whenever(FoodTruckRepository.getFoodTruckes(any())).doReturn(Success(listOf()))

            val useCase = DefaultGetFoodTruckUseCase(FoodTruckRepository)

            val FoodTruckViewModel = FoodTruckViewModel(
                this@FoodTruckViewModelTest.coroutineContext, useCase
            )

            FoodTruckViewModel.viewState().value shouldBeInstanceOf ViewState.Empty::class
        }
    }
    @Test
    fun `network error throws is error viewstate`() {
        runBlocking {
            val FoodTruckRepository: FoodTruckRepository = mock()
            whenever(FoodTruckRepository.getFoodTruckes(any())).doReturn(
                Failure(UnknownNetworkException)
            )

            val useCase = DefaultGetFoodTruckUseCase(FoodTruckRepository)

            val FoodTruckViewModel = FoodTruckViewModel(
                this@FoodTruckViewModelTest.coroutineContext, useCase
            )

            FoodTruckViewModel.viewState().value shouldBeInstanceOf ViewState.Error::class
        }
    }
}