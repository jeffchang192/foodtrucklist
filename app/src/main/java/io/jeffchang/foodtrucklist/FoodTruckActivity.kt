package io.jeffchang.foodtrucklist

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController


class FoodTruckActivity : AppCompatActivity() {

    private enum class MapState {
        LIST, MAP
    }

    private var mapState = MapState.LIST

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_truck)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_map -> {
                if (mapState == MapState.LIST) {
                    mapState = MapState.MAP
                    item.title = getString(R.string.list)
                } else {
                    mapState = MapState.LIST
                    item.title = getString(R.string.map)
                }
                navigate(mapState)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun navigate(mapState: MapState) {
        val navController = findNavController(R.id.nav_host_fragment)
        val destination = if (mapState == MapState.MAP) {
            R.id.MapFragment
        } else {
            R.id.FoodTruckFragment
        }
        navController.navigate(destination)
    }

}
