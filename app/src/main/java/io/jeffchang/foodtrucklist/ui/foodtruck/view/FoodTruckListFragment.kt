package io.jeffchang.foodtrucklist.ui.foodtruck.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.jeffchang.core.data.ViewState
import io.jeffchang.foodtrucklist.R
import io.jeffchang.foodtrucklist.databinding.FragmentFoodTruckBinding
import io.jeffchang.foodtrucklist.ui.foodtruck.inject
import io.jeffchang.foodtrucklist.ui.foodtruck.view.adapter.FoodTruckListAdapter
import io.jeffchang.foodtrucklist.ui.foodtruck.viewmodel.FoodTruckViewModel
import javax.inject.Inject

class FoodTruckListFragment : Fragment() {

    private lateinit var binding: FragmentFoodTruckBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    // Kotlin delegate to lazily create viewmodels.
    private val foodTruckViewModel
            by navGraphViewModels<FoodTruckViewModel>(R.id.nav_graph) {
                viewModelFactory
            }

    private val foodTruckListAdapter by lazy {
        FoodTruckListAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inject()
        binding = FragmentFoodTruckBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            foodTruckListRecyclerView.adapter = foodTruckListAdapter
            foodTruckListRecyclerView.addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            foodTruckListRecyclerView.layoutManager = LinearLayoutManager(context)
            foodTruckListRecyclerView.setHasFixedSize(true)
            restoreRecyclerViewPosition()
            swipeRefreshLayout.setOnRefreshListener {
                foodTruckViewModel.getFoodTrucks()
            }
        }
        subscribeUI()
        initFoodTrucks()
    }

    private fun restoreRecyclerViewPosition() {
        binding.foodTruckListRecyclerView.layoutManager
            ?.onRestoreInstanceState(foodTruckViewModel.instanceState.listPosition)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        foodTruckViewModel.instanceState = foodTruckViewModel.instanceState.copy(
            listPosition = binding.foodTruckListRecyclerView.layoutManager?.onSaveInstanceState()
        )
    }

    private fun initFoodTrucks() {
        foodTruckViewModel.getFoodTrucks()
    }

    private fun subscribeUI() {
        binding.apply {
            // Hides main UI with list. Shows Progress Bar.
            fun hide() {
                progressBar.isVisible = true
                foodTruckListRecyclerView.isVisible = false
            }

            // Shows main UI with list
            fun show() {
                progressBar.isVisible = false
                foodTruckListRecyclerView.isVisible = true
            }

            foodTruckViewModel.viewState().observe(viewLifecycleOwner, Observer {
                binding.swipeRefreshLayout.isRefreshing = false
                show()
                when (it) {
                    is ViewState.Success -> {
                        show()
                        foodTruckListAdapter.submitList(it.data)
                    }
                    is ViewState.Loading -> {
                        hide()
                    }
                    is ViewState.Empty -> {
                        show()

                    }
                    is ViewState.Error -> {
                        show()
                    }
                }
            })
        }
    }
}
