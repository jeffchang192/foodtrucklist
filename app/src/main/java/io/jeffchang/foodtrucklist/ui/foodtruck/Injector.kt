package io.jeffchang.foodtrucklist.ui.foodtruck

import io.jeffchang.foodtrucklist.coreComponent
import io.jeffchang.foodtrucklist.ui.foodtruck.di.DaggerFoodTruckComponent
import io.jeffchang.foodtrucklist.ui.foodtruck.view.FoodTruckListFragment

// Injector for FoodTruck fragment.
fun FoodTruckListFragment.inject() {
    DaggerFoodTruckComponent.builder()
        .coreComponent(coreComponent())
        .build()
        .inject(this)
}

