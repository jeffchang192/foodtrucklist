package io.jeffchang.foodtrucklist.ui.foodtruck.viewmodel

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.CameraPosition
import io.jeffchang.core.ContextProvider
import io.jeffchang.core.data.ViewState
import io.jeffchang.core.onFailure
import io.jeffchang.core.onSuccess
import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.foodtrucklist.ui.foodtruck.usecase.DefaultGetFoodTruckUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


data class InstanceState(
    val listPosition: Parcelable? = null,
    val cameraPosition: CameraPosition? = null
)

class FoodTruckViewModel @Inject constructor(
    private val contextProvider: ContextProvider,
    private val getFoodTruckUseCase: DefaultGetFoodTruckUseCase
) : ViewModel() {

    private val viewState = MutableLiveData<ViewState<List<FoodTruck>>>()

    fun viewState(): LiveData<ViewState<List<FoodTruck>>> = viewState

    var instanceState = InstanceState()

    fun getFoodTrucks() {
        if (viewState.value != null) {
            viewState.postValue(viewState.value)
            return
        }
        viewState.postValue(ViewState.Loading())
        launch {
            getFoodTruckUseCase()
                .onSuccess {
                    if (it.isEmpty()) {
                        viewState.postValue(ViewState.Empty())
                        return@onSuccess
                    }
                    Timber.d("Received food trucks")
                    viewState.postValue(ViewState.Success(it))
                }
                .onFailure {
                    viewState.postValue(ViewState.Error(it))
                }
        }
    }

    private fun launch(
        coroutineContext: CoroutineContext = contextProvider.main,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return viewModelScope.launch(coroutineContext) { block() }
    }

}
