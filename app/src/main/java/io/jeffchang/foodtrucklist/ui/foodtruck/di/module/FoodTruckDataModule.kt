package io.jeffchang.foodtrucklist.ui.foodtruck.di.module

import dagger.Module
import dagger.Provides
import io.jeffchang.core.ContextProvider
import io.jeffchang.core.scope.FeatureScope
import io.jeffchang.foodtrucklist.data.service.FoodTruckService
import io.jeffchang.foodtrucklist.ui.foodtruck.repository.DefaultFoodTruckRepository
import io.jeffchang.foodtrucklist.ui.foodtruck.repository.FoodTruckRepository
import io.jeffchang.foodtrucklist.ui.foodtruck.usecase.DefaultGetFoodTruckUseCase
import retrofit2.Retrofit

@Module
class FoodTruckDataModule {
    @Provides
    @FeatureScope
    fun provideFoodTruckService(retrofit: Retrofit): FoodTruckService =
        retrofit.create(FoodTruckService::class.java)

    @Provides
    @FeatureScope
    fun provideFoodTruckRepository(
        contextProvider: ContextProvider,
        FoodTruckService: FoodTruckService
    ): FoodTruckRepository =
        DefaultFoodTruckRepository(contextProvider, FoodTruckService)

    @Provides
    @FeatureScope
    fun provideGetNASAFoodTrucksUseCase(
        FoodTruckRepository: FoodTruckRepository
    ): DefaultGetFoodTruckUseCase = DefaultGetFoodTruckUseCase(FoodTruckRepository)

}
