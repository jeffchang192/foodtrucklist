package io.jeffchang.foodtrucklist.ui.foodtruck.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.jeffchang.core.di.ViewModelKey
import io.jeffchang.foodtrucklist.ui.foodtruck.viewmodel.FoodTruckViewModel

@Module
abstract class FoodTruckModule {

    @Binds
    @IntoMap
    @ViewModelKey(FoodTruckViewModel::class)
    abstract fun bindFoodTruckViewModel(viewModel: FoodTruckViewModel): ViewModel

}
