package io.jeffchang.foodtrucklist.ui.foodtruck.usecase

import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.foodtrucklist.ui.foodtruck.repository.FoodTruckRepository
import io.jeffchang.core.Result

class DefaultGetFoodTruckUseCase(private val FoodTruckRepository: FoodTruckRepository) :
    GetFoodTruckUseCase {

    override suspend fun invoke(): Result<List<FoodTruck>> {
        return FoodTruckRepository.getFoodTruckes()
    }

}