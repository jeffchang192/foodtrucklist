package io.jeffchang.foodtrucklist.ui.foodtruck.repository

import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.core.Result


interface FoodTruckRepository {

    suspend fun getFoodTruckes(): Result<List<FoodTruck>>

}
