package io.jeffchang.foodtrucklist.ui.map

import android.os.Bundle
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.jeffchang.core.data.ViewState
import io.jeffchang.foodtrucklist.R
import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.foodtrucklist.databinding.FragmentMapBinding
import io.jeffchang.foodtrucklist.ui.foodtruck.viewmodel.FoodTruckViewModel
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

// Constant of the boundaries to where the user can pan their map camera.
private const val LAT_LNG_BOUNDARY = 0.08
private const val UNION_SQUARE_LAT = 37.787994
private const val UNION_SQUARE_LONG = -122.407437

private const val MAP_TAG = "MAP_TAG"


class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var map: GoogleMap

    private var bottomSheet: BottomSheetDialog? = null

    private lateinit var binding: FragmentMapBinding

    private val atomicInteger = AtomicInteger()

    private var foodTrucks: Array<FoodTruck?>? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    // Kotlin delegate to lazily create viewmodels.
    private val foodTruckViewModel by navGraphViewModels<FoodTruckViewModel>(
        R.id.nav_graph
    ) {
        viewModelFactory
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inject()
        binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMapFragment()
        subscribeUI()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        foodTruckViewModel.instanceState = foodTruckViewModel.instanceState.copy(
            cameraPosition = map.cameraPosition
        )
    }

    private fun initMapFragment() {
        childFragmentManager
            .beginTransaction()
            .replace(
                R.id.mapFragment,
                SupportMapFragment.newInstance(),
                MAP_TAG
            )
            .commit()
    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map
        val viewState = foodTruckViewModel.viewState().value
        if (viewState is ViewState.Success) {
            foodTrucks = Array(viewState.data.size) { null }

            viewState.data
                .forEach {
                    val latLng = LatLng(
                        it.latitude.toDouble(),
                        it.longitude.toDouble()
                    )
                    map.addMarker(
                        MarkerOptions()
                            .position(latLng)
                            .title(it.applicant)
                            .snippet(it.optionaltext)

                    ).apply {
                        val id = atomicInteger.getAndIncrement()
                        tag = id
                        foodTrucks?.set(id, it)
                    }
                }
        }

        val camera = foodTruckViewModel.instanceState?.cameraPosition
            ?: CameraPosition.Builder()
                .target(
                    LatLng(
                        UNION_SQUARE_LAT,
                        UNION_SQUARE_LONG
                    )
                )
                .zoom(12.5f)
                .bearing(0f)
                .tilt(0f)
                .build()

        val center =
            CameraUpdateFactory.newCameraPosition(
                camera
            )
        map.moveCamera(center)
        map.setOnMarkerClickListener {
            showBottomSheet(it.tag as Int)
            return@setOnMarkerClickListener true
        }
        map.setLatLngBoundsForCameraTarget(
            LatLngBounds(
                LatLng(
                    UNION_SQUARE_LAT - LAT_LNG_BOUNDARY,
                    UNION_SQUARE_LONG - LAT_LNG_BOUNDARY
                ),
                LatLng(
                    UNION_SQUARE_LAT + LAT_LNG_BOUNDARY,
                    UNION_SQUARE_LONG + LAT_LNG_BOUNDARY
                )
            )
        )
    }

    private fun showBottomSheet(tag: Int) {
        val px = TypedValue.applyDimension(
            COMPLEX_UNIT_DIP,
            16f,
            requireContext().resources.displayMetrics
        )
            .toInt()
        foodTrucks?.get(tag)?.let {
            bottomSheet?.dismiss()
            bottomSheet = BottomSheetDialog(requireContext())
                .apply {
                    setContentView(
                        R.layout.item_food_truck
                    )
                    findViewById<TextView>(R.id.foodTruckTextView)?.text = it.applicant
                    findViewById<TextView>(R.id.addressTextView)?.text = it.location
                    findViewById<TextView>(R.id.bodyTextView)?.text = it.optionaltext
                    findViewById<TextView>(R.id.hoursTextView)?.text =
                        context.getString(R.string.hours, it.starttime, it.endtime)
                    findViewById<LinearLayout>(R.id.rootLayout)?.layoutParams =
                        FrameLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                            .apply {
                                setMargins(px, px, px, px)
                            }
                }
                .apply {
                    show()
                }

        }
    }

    private fun subscribeUI() {
        binding.apply {
            foodTruckViewModel.viewState().observe(viewLifecycleOwner, Observer {
                when (it) {
                    is ViewState.Success -> {
                        val mapFragment =
                            childFragmentManager.findFragmentByTag(MAP_TAG) as SupportMapFragment
                        mapFragment.getMapAsync(this@MapFragment)
                    }
                    is ViewState.Error -> {
                        Toast.makeText(
                            context,
                            R.string.loading_places_error,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })
        }
    }
}
