package io.jeffchang.foodtrucklist.ui.foodtruck.repository

import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.foodtrucklist.data.service.FoodTruckService
import io.jeffchang.core.ContextProvider
import io.jeffchang.core.Result
import io.jeffchang.core.safeApiCall
import kotlinx.coroutines.withContext

class DefaultFoodTruckRepository(
    private val provider: ContextProvider,
    private val foodTruckService: FoodTruckService
) : FoodTruckRepository {

    override suspend fun getFoodTruckes(): Result<List<FoodTruck>> {
        return withContext(provider.io) {
            safeApiCall {
                foodTruckService.getFoodTrucks()
            }
        }
    }
}