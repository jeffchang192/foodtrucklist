package io.jeffchang.foodtrucklist.ui.foodtruck.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import io.jeffchang.foodtrucklist.R
import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.foodtrucklist.databinding.ItemFoodTruckBinding

class FoodTruckListAdapter
    : ListAdapter<FoodTruck, FoodTruckListAdapter.FoodTruckViewHolder>(FoodTruckDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodTruckViewHolder {
        val binding = ItemFoodTruckBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return FoodTruckViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FoodTruckViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class FoodTruckViewHolder(private val binding: ItemFoodTruckBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(foodTruck: FoodTruck) {
            binding.apply {
                val context = root.context

                // Sets fields or use default values.
                this.addressTextView.text = foodTruck.location
                this.bodyTextView.text = foodTruck.optionaltext
                this.foodTruckTextView.text = foodTruck.applicant

                this.hoursTextView.text =
                    context.getString(R.string.hours, foodTruck.starttime, foodTruck.endtime)
            }
        }
    }

    private class FoodTruckDiffCallback : DiffUtil.ItemCallback<FoodTruck>() {
        override fun areItemsTheSame(oldItem: FoodTruck, newItem: FoodTruck): Boolean =
            oldItem === newItem

        override fun areContentsTheSame(oldItem: FoodTruck, newItem: FoodTruck): Boolean =
            oldItem == newItem
    }
}
