package io.jeffchang.foodtrucklist.ui.map

import io.jeffchang.foodtrucklist.coreComponent
import io.jeffchang.foodtrucklist.ui.foodtruck.di.DaggerFoodTruckComponent

// Injector for FoodTruck fragment.
fun MapFragment.inject() {
    DaggerMapComponent.builder()
        .coreComponent(coreComponent())
        .build()
        .inject(this)
}

