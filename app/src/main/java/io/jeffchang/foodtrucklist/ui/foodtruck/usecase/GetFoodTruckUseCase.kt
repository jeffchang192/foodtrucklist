package io.jeffchang.foodtrucklist.ui.foodtruck.usecase

import io.jeffchang.foodtrucklist.data.model.FoodTruck
import io.jeffchang.core.Result


interface GetFoodTruckUseCase {

    suspend operator fun invoke(): Result<List<FoodTruck>>

}