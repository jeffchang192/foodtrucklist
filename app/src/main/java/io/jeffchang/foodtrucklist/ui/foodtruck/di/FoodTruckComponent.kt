package io.jeffchang.foodtrucklist.ui.foodtruck.di

import dagger.Component
import io.jeffchang.core.CoreComponent
import io.jeffchang.core.di.BaseFragmentComponent
import io.jeffchang.core.di.module.ViewModelModule
import io.jeffchang.core.scope.FeatureScope
import io.jeffchang.foodtrucklist.ui.foodtruck.di.module.FoodTruckDataModule
import io.jeffchang.foodtrucklist.ui.foodtruck.di.module.FoodTruckModule
import io.jeffchang.foodtrucklist.ui.foodtruck.view.FoodTruckListFragment

/**
 * Component binding injections for FoodTruck related classes
 */
@Component(
    modules = [ViewModelModule::class, FoodTruckModule::class, FoodTruckDataModule::class],
    dependencies = [CoreComponent::class]
)
@FeatureScope
interface FoodTruckComponent : BaseFragmentComponent<FoodTruckListFragment> {

    @Component.Builder
    interface Builder {

        fun coreComponent(component: CoreComponent): Builder

        fun build(): FoodTruckComponent
    }
}
