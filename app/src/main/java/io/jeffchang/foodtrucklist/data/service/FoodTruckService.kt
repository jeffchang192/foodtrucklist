package io.jeffchang.foodtrucklist.data.service

import io.jeffchang.foodtrucklist.data.model.FoodTruck
import retrofit2.http.GET
import retrofit2.http.Url

interface FoodTruckService {

    @GET
    suspend fun getFoodTrucks(
        @Url url: String = "https://data.sfgov.org/resource/jjew-r69b.json"
    ): List<FoodTruck>

}
