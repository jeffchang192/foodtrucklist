package io.jeffchang.foodtrucklist.data.model


data class FoodTruck(
    val applicant: String,
    val latitude: String,
    val longitude: String,
    val location: String,
    val optionaltext: String? = null,
    val starttime: String,
    val endtime: String
)
